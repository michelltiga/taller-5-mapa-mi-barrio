class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligono=[];

    constructor(){

        this.posicionInicial=[4.634398,-74.190803];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };

        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }
    colocarMarcador(posicion){
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }
    colocarCirculo(posicion, configuracion){
        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);
    }
    colocarPoligono(posicion, configuracion){
        this.poligono.push(L.polygon(posicion, configuracion));
        this.poligono[this.poligono.length-1].addTo(this.miVisor);
    }
}

let miMapa=new Mapa();

// marcador y circulo casa

miMapa.colocarMarcador([4.634398,-74.190803]);
miMapa.colocarCirculo([4.634398,-74.190803], {
    color: 'red',
    fillColor: '#f46',
    fillOpacity: 0.5,
    radius: 130
});

//nuevos marcadores y circulos en el mapa 

miMapa.colocarMarcador([4.710989,-74.072090]);
miMapa.colocarCirculo([4.710989,-74.072090], {
    color: 'red',
    fillColor: '#f46',
    fillOpacity: 0.5,
    radius: 130
});

miMapa.colocarMarcador([4.635286,-74.145171]);
miMapa.colocarCirculo([4.635286,-74.145171], {
    color: 'red',
    fillColor: '#f46',
    fillOpacity: 0.5,
    radius: 130
});
miMapa.colocarMarcador([4.627757,-74.122497]);
miMapa.colocarCirculo([4.627757,-74.122497], {
    color: 'red',
    fillColor: '#f46',
    fillOpacity: 0.5,
    radius: 130
});


//poligono colegio cercano 

miMapa.colocarPoligono([[4.633516, -74.189993],[4.632992, -74.189681],[4.633601, -74.188361],[4.634099, -74.188570]], {
    color: 'red',
    fillColor: '#f46',
    fillOpacity: 0.5,
});


